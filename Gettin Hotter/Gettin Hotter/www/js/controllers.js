angular.module('starter')

.controller('LoginCtrl', function ($scope, $localStorage, $location, $cordovaOauth) {
    delete $localStorage.facebookAccessToken;
    delete $localStorage.googleAccessToken;

    $scope.loginFacebook = function () {
        $cordovaOauth.facebook("485581964926921", ["email", "read_stream", "user_website", "user_location", "user_relationships", "user_friends"]).then(function (result) {
            $localStorage.facebookAccessToken = result.access_token;
            $location.path("/tab/profile");
        }, function (error) {
            $localStorage.facebookAccessToken = "CAAG5olyHX8kBABgYixbANWeqY8D482p8Ym8kjz687U3cFGCkZCJxjHmqv7pvDBiwfjtJgZCAt0M5pUVyZA74vJjNPzLAfYUfjTZBmLVbVwPFnZA5wyldRfUAzQZCkZBUGZALeM5VxwZCfXuRNk8JPe3BSIuZBnCch6ZCdCf7aG1zBRBn4TX4m6ZCFqpo";
            console.log(error);
            $location.path("/tab/profile");
            alert("There was a problem signing in!  See the console for logs");
        });
    };

    $scope.loginGoogle = function () {
        $cordovaOauth.google("225501462092-6eqav3c3invmo52so4no6fh7ja8aq8ls.apps.googleusercontent.com", ["openid", "https://www.googleapis.com/auth/plus.login", "email"]).then(function (result) {
            $localStorage.googleAccessToken = result.access_token;
            $location.path("/tab/profile");
        }, function (error) {
            console.log(error);
            $location.path("/tab/profile");
            alert("There was a problem signing in!  See the console for logs");
        });
    };
})

.controller('ChatsCtrl', function($scope, Chats) {
  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  }
})

.controller('ChatDetailCtrl', function ($scope, $stateParams, Chats, $timeout, $q, $localStorage, $ionicPlatform, $http) {
    $ionicPlatform.ready(function () {

        $scope.chat = Chats.get($stateParams.chatId);
        var root = 'http://gettinhotter.com/';
        $scope.$storage = $localStorage;
        $scope.track_id = '47';
        $scope.start_track_bool = true;
        $scope.my_location = {};
        $scope.your_location = {};
        $scope.distance = '';

        //Arrow math
        var g_PI2 = Math.PI * 2;   // 2 x Pi
        var g_toRAD = 360 / g_PI2; // Divide degrees by this to get radians
        var g_watchID = null;    // the current `watchHeading`
        var g_ArrHeight = 48;

        //Toggle-function for tracking selected ID
        $scope.trackingToggle = function (id) {
            $scope.track_id = id;
            $scope.start_track_bool = !$scope.start_track_bool;
        }

        //Get a new userID if you dont have one
        if (!$scope.$storage.user_id) {
            $http.get(root + 'api/getAvailibleId.php').then(function (result) {
                $scope.$storage.user_id = result.next_availible_id;
            });
        }

        //Loop checking current compass-heading and updating the arrow
        navigator.compass.watchHeading(function (heading) {
            var my_math = 360 - heading.trueHeading + calcBearing($scope.my_location.lat, $scope.my_location.long, $scope.your_location.lat, $scope.your_location.long);
            drawArrow(g_PI2 - my_math / g_toRAD);
        });
        function calcBearing(lat1, lng1, lat2, lng2) {
            var dLon = (lng2 - lng1);
            var y = Math.sin(dLon) * Math.cos(lat2);
            var x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);
            var brng = Math.atan2(y, x) * 180 / Math.PI;
            return ((brng + 360.0) % 360.0);
        }

        //Get location (long/lat) of tracking id
        function getCoordinates(id) {
            $http.get(root + "api/getCoordinates.php", { params: { receiver_id: id } }).success(function (result) {
                $scope.your_location = result;
            });
        }
        //Send location to server
        function sendCoordinates(id, long, lat) {
            $http.get(root + "api/sendCoordinates.php", { params: { sender_id: id, long: long, lat: lat } });
        }
        //Get the device's current position
        navigator.geolocation.watchPosition(function (r) {
            var pos = {};
            pos.long = r.coords.longitude;
            pos.lat = r.coords.latitude;
            $scope.my_location = pos;
        }, function () {
            return 'Error getting location';
        }, { enableHighAccuracy: true });
        
        //Calulate distance between 2 long/lat points
        function calcDistance(lat1, lon1, lat2, lon2) {
            var R = 6371;
            var a =
               0.5 - Math.cos((lat2 - lat1) * Math.PI / 180) / 2 +
               Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
               (1 - Math.cos((lon2 - lon1) * Math.PI / 180)) / 2;

            var distance = R * 2 * Math.asin(Math.sqrt(a));

            if (distance < 10) {
                //convert km to meters.
                return 'Meters: ' +  Math.round(distance * 1000);
            }
            else{
                return 'Km: ' +  Math.round(distance);
            }
        }
        //Timer for calling tracking functions
        $timeout(function getNewCoords() {
            $timeout(getNewCoords, 1000);
            if ($scope.start_track_bool) {
                sendCoordinates($scope.$storage.user_id, $scope.my_location.long, $scope.my_location.lat);
                $scope.distance = calcDistance($scope.my_location.lat, $scope.my_location.long, $scope.your_location.lat, $scope.your_location.long);
                getCoordinates($scope.track_id);
            }
        });

        //Return address from long/lat.
        function reverseGeocode(long, lat) {
            var deferred = $q.defer();
            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(lat, long);
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    deferred.resolve(results[0].formatted_address);
                }
                else {
                    deferred.reject(status);
                }
            });
            return deferred.promise;
        }

        //Function for drawing the arrow (hurrderp)
        function drawArrow(r) {
            var ctx = document.getElementById('arrow').getContext('2d');
            ctx.clearRect(0, 0, g_ArrHeight * 4, g_ArrHeight * 4);
            var state = ctx.save();
            var fulld3 = g_ArrHeight / 3;
            var fulld2 = g_ArrHeight / 2;
            ctx.translate(g_ArrHeight * 2, g_ArrHeight * 2);
            ctx.rotate(-r);

            ctx.beginPath();
            ctx.strokeStyle = '#FF0606';
            ctx.lineWidth = 5;

            ctx.moveTo(0, -g_ArrHeight);
            ctx.lineTo(g_ArrHeight, fulld3);
            ctx.lineTo(fulld2, fulld3);
            ctx.lineTo(fulld2, g_ArrHeight);
            ctx.lineTo(-fulld2, g_ArrHeight);
            ctx.lineTo(-fulld2, fulld3);
            ctx.lineTo(-g_ArrHeight, fulld3);

            ctx.closePath();
            ctx.stroke();
            ctx.fillStyle = '#58E3FF';
            ctx.fill();

            ctx.restore(state);
        }

    });

})

.controller('AccountCtrl', function ($scope, $cordovaOauth, $localStorage, $location) {
  $scope.settings = {
    enableFriends: true
  };
})

.controller('TabsCtrl', function ($scope, $rootScope, $state) {
    $rootScope.$on('$ionicView.beforeEnter', function () {
        $rootScope.hideTabs = false;

        if ($state.current.name === 'tab.login') {
            $rootScope.hideTabs = true;
        }
    });
})

.controller("ProfileCtrl", function ($scope, $http, $localStorage, $location) {
    https://www.googleapis.com/plus/v1/people/me/people/connected?
    $scope.init = function () {
        if ($localStorage.hasOwnProperty("facebookAccessToken") === true) {
            $http.get("https://graph.facebook.com/v2.3/me", { params: { access_token: $localStorage.facebookAccessToken, fields: "id,name,gender,location,website,picture,relationship_status" } }).then(function (result) {
                $scope.profileData = result.data;
                console.log(result);
            }, function (error) {
                alert("There was a problem getting your profile.  Check the logs for details.");
                console.log(error);
            });
            $http.get("https://graph.facebook.com/v2.3/me/friends", { params: { access_token: $localStorage.facebookAccessToken } }).then(function (result) {
                console.log(result);
            }, function (error) {
                alert("There was a problem getting your Friends.  Check the logs for details.");
                console.log(error);
            });
        } else if ($localStorage.hasOwnProperty("googleAccessToken") === true) {
            $http.get("https://www.googleapis.com/oauth2/v1/userinfo", { params: { access_token: $localStorage.googleAccessToken } }).then(function (result) {
                $scope.profileData = result.data;
                console.log(result);
            }, function (error) {
                alert("There was a problem getting your profile.  Check the logs for details.");
                console.log(error);
            });
            $http.get("https://www.googleapis.com/plus/v1/people/me/people/connected", { params: { access_token: $localStorage.googleAccessToken } }).then(function (result) {
                console.log(result);
            }, function (error) {
                alert("There was a problem getting your Friends.  Check the logs for details.");
                console.log(error);
            });
        } else {
            alert("Not signed in");
            $location.path("/tab/login");
        }
    };

});

